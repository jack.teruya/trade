import random
import uuid
from datetime import datetime
from time import sleep

from src.domain.trade import Trade
from src.repository.memory import Memory


class GeradorTrades:
    def __init__(self, ativos: list, quantidade: int, repository=Memory) -> None:
        self.ativos = ativos
        self.quantidade = quantidade
        self.repository = repository

    def gerar_trades(self):
        for x in range(self.quantidade):
            val = random.randint(0, len(self.ativos)-1)
            trade = Trade(
                ativo=self.ativos[val],
                data=datetime.today(),
                preco=round(random.uniform(1.0, 100.0), 2),
                id=uuid.uuid4()
            )

            self.repository().incluir(trade.to_dict())
