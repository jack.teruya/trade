from src.repository.memory import Memory


class TradeUseCase:
    def __init__(self, repository=Memory):
        self.repository = repository

    def list_all(self):
        try:
            trades = self.repository().list()
            return trades

        except Exception as exc:
            return exc

    def list_de_ativo(self, trade_data):
        try:
            trades = []
            for trade in self.repository().list():
                if trade_data["ativo"] == trade.__dict__["ativo"]:
                    trades.append(trade)

            return trades

        except Exception as exc:
            return exc

    def list_preco_maior(self, trade_data):
        try:
            trades = []
            for trade in self.repository().list():
                if trade_data["preco"] < trade.__dict__["preco"]:
                    trades.append(trade)

            return trades

        except Exception as exc:
            return exc

    def list_preco_maior_com_ativo(self, trade_data):
        try:
            trades = []
            for trade in self.repository().list():
                if trade_data["ativo"] == trade.__dict__["ativo"] and \
                        trade_data["preco"] < trade.__dict__["preco"]:

                    trades.append(trade)

            return trades

        except Exception as exc:
            return exc

    def list_preco_menor(self, trade_data):
        try:
            trades = []
            for trade in self.repository().list():
                if trade_data["preco"] > trade.__dict__["preco"]:
                    trades.append(trade)

            return trades

        except Exception as exc:
            return exc

    def list_preco_menor_com_ativo(self, trade_data):
        try:
            trades = []
            for trade in self.repository().list():
                if trade_data["ativo"] == trade.__dict__["ativo"] and \
                        trade_data["preco"] > trade.__dict__["preco"]:
                    trades.append(trade)

            return trades

        except Exception as exc:
            return exc

    def list_recentes_ativo(self, trade_data, quantidade=5):
        try:
            trades = []
            for trade in self.repository().list():
                status = True
                if len(trades) == 0 and trade_data["ativo"] == trade.__dict__["ativo"]:
                    trades.append(trade)
                if trade_data["ativo"] == trade.ativo:
                    for trade_ in trades:
                        if trade_.data < trade.data and status:
                            index = trades.index(trade_)
                            trades.insert(index, trade)
                            status = False
                        if len(trades) > quantidade:
                            trades.pop(5)
            return trades

        except Exception as exc:
            return exc

    def list_recentes(self):
        try:
            trades = []
            for trade in self.repository().list():
                status = True
                if len(trades) == 0:
                    trades.append(trade)
                for trade_ in trades:
                    if status and trade_.data < trade.data:
                        index = trades.index(trade_)
                        trades.insert(index, trade)
                        status = False
                    if len(trades) > 5:
                        trades.pop(5)
            return trades

        except Exception as exc:
            return exc