from src.domain.trade import Trade


class Memory:
    def __init__(self, data=[]):
        self.data = data

    def incluir(self, trade):
        self.data.append(trade)

    def list(self):
        return [Trade.from_dict(i) for i in self.data]

