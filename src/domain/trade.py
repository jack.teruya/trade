import dataclasses
from datetime import datetime
from uuid import UUID


@dataclasses.dataclass
class Trade:
    ativo: str
    data: datetime
    preco: float
    id: UUID


    # def __init__(self, ativo_negociado: str, data: datetime, preco: float, id: UUID):
    #     self.ativo_negociado = ativo_negociado
    #     self.data = data
    #     self.preco = preco
    #     self.id = id

    @classmethod
    def from_dict(cls, d):
        return cls(**d)

    def to_dict(self):
        return dataclasses.asdict(self)
