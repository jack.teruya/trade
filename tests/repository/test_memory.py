import uuid
from datetime import datetime

import pytest

from src.domain.trade import Trade
from src.repository.memory import Memory


@pytest.fixture
def trade_dicts():
    return [
        {
            "id": uuid.uuid4(),
            "ativo": "TEST1",
            "preco": 70.0,
            "data": datetime.today()
        },
        {
            "id": uuid.uuid4(),
            "ativo": "TEST2",
            "preco": 70.0,
            "data": datetime.today()
        },
        {
            "id": uuid.uuid4(),
            "ativo": "TEST3",
            "preco": 70.0,
            "data": datetime.today()
        },
        {
            "id": uuid.uuid4(),
            "ativo": "TEST4",
            "preco": 70.0,
            "data": datetime.today()
        },
        {
            "id": uuid.uuid4(),
            "ativo": "TEST5",
            "preco": 70.0,
            "data": datetime.today()
        }
    ]


def test_repository_memory(trade_dicts):
    repo = Memory(trade_dicts)

    trades = [Trade.from_dict(i) for i in trade_dicts]

    assert repo.list() == trades
