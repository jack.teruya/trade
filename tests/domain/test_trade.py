import uuid
from datetime import datetime

import pytest

from src.domain.trade import Trade


@pytest.fixture
def trade_dict():
    id = uuid.uuid4()
    data = datetime.today()
    return {
        "id": id,
        "ativo": "TEST1",
        "preco": 70.0,
        "data": data
    }


def test_trade_from_dict(trade_dict):
    trade1 = Trade.from_dict(trade_dict)
    trade2 = Trade.from_dict(trade_dict)

    assert trade1.id == trade2.id
    assert trade1.data == trade2.data
    assert trade1.ativo == trade2.ativo
    assert trade1.preco == trade2.preco


def test_trade_to_dict(trade_dict):
    trade1 = Trade(
        trade_dict["ativo"],
        trade_dict["data"],
        trade_dict["preco"],
        trade_dict["id"]
    )
    trade2 = Trade(
        trade_dict["ativo"],
        trade_dict["data"],
        trade_dict["preco"],
        trade_dict["id"]
    )

    trade1_dict = trade1.to_dict()
    trade2_dict = trade2.to_dict()

    assert trade1_dict == trade2_dict
