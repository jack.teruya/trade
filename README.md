#TradeBot 
versão o python é a 3.10

Iniciando o Projeto com docker:

    $ sudo docker build -t trader .
    $ sudo docker run -it trader cli.py

Com do docker instalado, caso não tenha -> https://docs.docker.com/engine/install/ 


Se preferir iniciar o projeto sem docker, segue passos abaixo:

-Crie um ambiente virtual para desenvolvimento e ative:
    
    $ python3 -m venv .env
    $ source .env/bin/activate


-Instalando os requirements:
    
    $ pip install -r requirements.txt;

-Só iniciar o projeto: $ python3 cli.py
