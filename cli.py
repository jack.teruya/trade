from src.repository.memory import Memory
from src.use_case.trade import TradeUseCase
from src.use_case.trades_generator import GeradorTrades


def gerar_trades():
    lista_ativos = str(input("ativos: ")).split(",")
    lista_ativos = [ativo.replace(" ", "", 5) for ativo in lista_ativos]
    qtd = int(input("quantidade: "))
    gerar_trades = GeradorTrades(lista_ativos, qtd, Memory)
    gerar_trades.gerar_trades()


def gerar_trades_novamente():
    print("Deseja gerar mais trades? Sim/S para continuar ou qualquer outro valor para continuar")
    resposta = input("R: ")
    while resposta.lower() in ["sim", "s"]:
        gerar_trades()
        resposta = input("R: ")
    print("\n")


def escolher_opcao_de_consulta():
    status = True
    while status:
        print("Escolha o numero das opções a seguir: ")
        print("1 - listar todas as trades")
        print("2 - Listar todos os Trades de um ativo especifico")
        print("3 - Listar todos os Trades com um preço de execução maior que um determinado valor")
        print("4 - Listar todos os Trades de um ativo especifico com um preço de execução maior que um determinado valor")
        print("5 - Listar todos os Trades com um preço de execução menor que um determinado valor")
        print("6 - Listar todos os Trades de um ativo especifico com um preço de execução menor que um determinado valor")
        print("7 - Listar o Trade mais recente de um ativo específico")
        print("8 - Listar o Trade mais recente entre todos os Trades gerados")
        print("9 - Gerar mais trades")
        print("0 - Finalizar Bot")
        opcao = input("Opção: ")

        if str(opcao).isnumeric() and int(opcao) in [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]:
            status = False
            return int(opcao)


def gerar_metodo_trade_escolhida(trades, opcao):
    match opcao:
        case 1:
            list_trade = trades.list_all()
            for trade in list_trade:
                print(trade.to_dict())
        case 2:
            ativo = input("Informe o ativo: ")
            list_trade = trades.list_de_ativo(dict(ativo=ativo))
            for trade in list_trade:
                print(trade.to_dict())
        case 3:
            preco = float(input("Informe o preco: "))
            list_trade = trades.list_preco_maior(dict(preco=preco))
            for trade in list_trade:
                print(trade.to_dict())
        case 4:
            preco = float(input("Informe o preco: "))
            ativo = input("Informe o ativo: ")
            list_trade = trades.list_preco_maior_com_ativo(dict(preco=preco, ativo=ativo))
            for trade in list_trade:
                print(trade.to_dict())
        case 5:
            preco = float(input("Informe o preco: "))
            list_trade = trades.list_preco_menor(dict(preco=preco))
            for trade in list_trade:
                print(trade.to_dict())
        case 6:
            preco = float(input("Informe o preco: "))
            ativo = input("Informe o ativo: ")
            list_trade = trades.list_preco_menor_com_ativo(dict(preco=preco, ativo=ativo))
            for trade in list_trade:
                print(trade.to_dict())
        case 7:
            ativo = input("Informe o ativo: ")
            list_trade = trades.list_recentes_ativo(dict(ativo=ativo))
            for trade in list_trade:
                print(trade.to_dict())
        case 8:
            list_trade = trades.list_recentes()
            for trade in list_trade:
                print(trade.to_dict())
        case 9:
            gerar_trades()
    input("Enter para continuar.")
    print("\n")


def escolher_metodos_trades():
    status = True
    trades = TradeUseCase(Memory)
    while status:
        opcao = escolher_opcao_de_consulta()
        if int(opcao) == 0:
            status = False
            return status
        gerar_metodo_trade_escolhida(trades, opcao)


def main():
    print("Olá, estamos iniciando TradeBot")
    print("Como inicio do bot, precisaremos de "
          "uma lista de ativo e a quantidade que será gerada")

    # Gerar Trades
    gerar_trades()

    # Gerar novamente trades, enquanto sim será necessario incluir
    gerar_trades_novamente()

    escolher_metodos_trades()


# PETR4, VALE3, WDOZ21, ALUP11

if __name__ == "__main__":
    main()
